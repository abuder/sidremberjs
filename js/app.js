App = Ember.Application.create();

App.Router.map(function() {
 	this.resource('item1');
 	this.resource('item2');
 	this.resource('item3');
});

App.ApplicationRoute = Ember.Route.extend({

  actions: {

  	showItem1: function(){
		$('#sidr-content > li').removeClass("active");
  		$.sidr();
  		$('#item1').addClass("active");
  		this.transitionTo("item1");
  	},

  	showItem2: function(){
  		$('#sidr-content > li').removeClass("active");
  		$.sidr();
  		$('#item2').addClass("active");
  		this.transitionTo("item2");
  	},

  	showItem3: function(){
  		$('#sidr-content > li').removeClass("active");
  		$.sidr();
  		$('#item3').addClass("active");
  		this.transitionTo("item3");
  	},
  },
});

App.IndexRoute = Ember.Route.extend({
  model: function() {
    return ['red', 'yellow', 'blue'];
  },
});

$(document).ready(function() {
  $('#simple-menu').sidr();
});